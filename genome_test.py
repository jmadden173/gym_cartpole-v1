#!/usr/bin/python3

import os
import neat
import gym
import numpy as np
import pickle



def run(config_file):
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                        neat.DefaultSpeciesSet, neat.DefaultStagnation,
                        config_file)

    print('\nTesting Best Genome:\n')
    
    env = gym.make('CartPole-v1')
    observation = env.reset()

    # Load the winner
    with open('winner-genome', 'rb') as f:
        winner = pickle.load(f)
    
    print('Loaded Genome:')
    print(winner)

    winner_net = neat.nn.FeedForwardNetwork.create(winner, config)
    while True:
        action = winner_net.activate(observation)
        action = round(action[0])
        observation, reward, done, info = env.step(action)

        print("Action: {}".format(action))
        print("Observation: {}".format(observation))
        print("Reward: {}".format(reward))

        env.render()
        
        if done:
            env.reset()
            print("\n\n------------------------- Finished -------------------------\n\n")
            input("Press enter to continue...")

    env.close()


if __name__ == "__main__":
    # Path of the config file in the
    # working directory
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, 'config-feedforward')
    run(config_path)
