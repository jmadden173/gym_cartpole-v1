#!/usr/bin/python3

import os
import gym
import numpy as np

env = gym.make('CartPole-v1')

env.reset()

steps = 0

fitness = 0

outputarray = []

#for _ in range(200):
while True:
    sample = env.action_space.sample()
    observation, reward, done, info = env.step(sample)

    outputarray.append(sample)

    fitness += reward

    print("Action: {}".format(sample))
    print("Observation: {}".format(observation))
    print("Reward: {}".format(reward))
    print("Info: {}".format(info))

    env.render()
   
    steps += 1

    #input("Press enter to continue...") 
    
    if done:
        env.reset()

        print("Steps: {}".format(steps))
        steps = 0

        print("Fitness: {}".format(fitness))
        fitness = 0

        output_min = np.amin(outputarray)
        print("Min Values: {}".format(output_min))

        output_max = np.amax(outputarray)
        print("Max Values: {}".format(output_max))

        print("\n--------------- RESET ---------------\n")
        input("Press enter to continue...")

env.close()
