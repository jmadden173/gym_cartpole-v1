#!/usr/bin/python3

import gym
import neat
import pickle
import numpy as np
import os
import time

env.gym.make('CartPole-v1')

def eval_genomes(genomes, config):
    for genome_id, genome in genomes:
        net = neat.nn.FeedForwardNetwork.create(genome, config)
        genome.fitness = 0

        observation = env.reset()
        done = False

        while not done:
            action = net.activate((observation[0], observation[1], observation[2], observation[3], ))
            action[0] = round(action[0])
            observation, reward, done, info = env.step(action[0])
            env.render()

            genome.fitness += reward


def run(config_file):
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction, neat.DefaultSpeciesSet,
            neat.DefaultStagnation, config_file)

    # Creates Population
    p = neat.Population(config)

    # Add console logging
    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)

    p.add_reporter(neat.Checkpointer(1000, 3600))

    winner = p.run(eval_genomes)

    print("Saving best genome")
    print(winner)

    with open('winner-genome', 'wb') as f:
        pickle.dump(winner,f)


if __name__ == "__main__":
    #Path of config file in current dir
    local_dir = os.path.dirname(__file__)
    config_path = os.path.join(local_dir, 'config-feedforward')
    run(config_path)
